package org.arkcris.foodtracker.product;

import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ProductsService {

    int id = 1;
    Map<Integer, Product> products = new HashMap<>();

    public ProductsService() {
        products.put(id, new Product(id++, "Mleko", "Mleko desc", "21.01.2022"));
        products.put(id, new Product(id++, "Kisiel", "Kisiel desc", "22.01.2022"));
        products.put(id, new Product(id++, "Woda", "Woda desc", "23.01.2022"));
        products.put(id, new Product(id++, "Soda", "Soda desc", "24.01.2022"));
    }

    public List<Product> getProducts() {
        // TODO: use DB
        return new ArrayList<>(products.values());
    }

    public Product getProduct(int id) {
        return products.get(id);
    }

    public void addProduct(Product product) {
        product.setId(id++);
        products.put(product.getId(), product);
    }

    public void deleteProduct(int id) {
        this.products.remove(id);
    }
}
