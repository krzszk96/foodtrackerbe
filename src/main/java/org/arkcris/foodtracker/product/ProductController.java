package org.arkcris.foodtracker.product;

import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("product")
public class ProductController {

    private final ProductsService productsService;

    ProductController(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping
    public List<Product> getProducts() {
        return productsService.getProducts();
    }

    @GetMapping("/{id}")
    public Product getProducts(@PathVariable int id) {
        return productsService.getProduct(id);
    }

    @PostMapping
    public List<Product> addProduct(@RequestBody Product product) {
        productsService.addProduct(product);
        return productsService.getProducts();
    }

    @DeleteMapping("/{id}")
    public List<Product> deleteProduct(@PathVariable int id) {
        productsService.deleteProduct(id);
        return productsService.getProducts();
    }
}
