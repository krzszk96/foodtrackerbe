package org.arkcris.foodtracker.product;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ProductControllerTest {

    @Mock
    ProductsService productsServiceMock;

    ProductController productController;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        int id = 1;
        Map<Integer, Product> products = new HashMap<>();
        products.put(id, new Product(id++, "Mleko", "Mleko desc", "21.01.2022"));
        products.put(id, new Product(id++, "Kisiel", "Kisiel desc", "21.01.2022"));
        products.put(id, new Product(id++, "Woda", "Woda desc", "21.01.2022"));
        when(productsServiceMock.getProducts()).thenReturn(new ArrayList<>(products.values()));

        productController = new ProductController(productsServiceMock);
    }

    @Test
    public void testProductController() {
        // given

        // when

        // then
    }

}